package com.darren.java.decorator;



import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

public class DecoratorTest {

	@Test
	public void testDecorator() {
		Sandwish sandwish = new DressingDecorator(new MeatDecorator(new SimpleSandwish()));
		assertThat(sandwish.make()).isEqualTo("Bread + trukey + mustard");
	}
}
