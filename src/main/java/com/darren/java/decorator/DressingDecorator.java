package com.darren.java.decorator;

public class DressingDecorator extends SandwishDecorator{

	public DressingDecorator(Sandwish customSanwish) {
		super(customSanwish);
	}

	public String make() {
		return customSanwish.make() + addDressing();
	}

	private String addDressing() {
		
		return " + mustard";
	}
}
