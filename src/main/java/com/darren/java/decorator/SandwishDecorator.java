package com.darren.java.decorator;

public class SandwishDecorator implements Sandwish{

	protected Sandwish customSanwish;
	
	public SandwishDecorator(Sandwish customSanwish) {
		super();
		this.customSanwish = customSanwish;
	}

	@Override
	public String make() {
		return customSanwish.make();
	}

}
